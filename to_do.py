#!/usr/bin/python3

import sys
import os

# When the script is finished, don't forget to change the relative path to the .config files
path = "./.list"

# Display the help screen when --help flag is called or when arg are invalid.
def help():
    print(
    """
    Usage:
    -h  --help          Display the usage panel
    -s  --show          See the to do list
    -a  --add           Append new event to the list.
    -d  --delete [ID]   Delete event specified by ID
    """)


def add():
    task = input("Add a new element\n")
    f = open(path, "a")
    f.write(task + "\n")
    f.close()

def show():
    f = open(path, "r")
    lines = f.readlines()
    for index, line in enumerate(lines, start=1):
        print(str(index) +"\t" + line)
    f.close()

def delete():
    show()
    option = input("Select the ID that you want to delete.\n\
Write 'all' if you want to delete everything.\n")
    if option.lower() == "all":
        confirmation = input("Are you sure? (y/N)\n")
        if confirmation.lower() == "y":
            os.remove(path)
        elif confirmation.lower() == "n":
            pass
        else:
            print("Select only 'y' or 'n'\n")
    elif option.isnumeric():
        option = int(option)
        f = open(path, "r")
        lines = f.readlines()
        lines.pop(option - 1)
        f.close()
        os.remove(path)
        for line in lines:
            f = open(path,"a")
            f.write(line)
            f.close()
    else:
        print("Error")

try:
    os.system('clear')
    if ("--help" == sys.argv[1])|("-h" == sys.argv[1]):
        help()
    elif ("--add" == sys.argv[1])|("-a" == sys.argv[1]):
        add()
    elif ("--show" == sys.argv[1])|("-s" == sys.argv[1]):
        show()
    elif ("--delete" == sys.argv[1])|("-d" == sys.argv[1]):
        delete()
    else:
        print("Invalid arguments")
        help()
except (KeyboardInterrupt, SystemExit):
    print("\nExiting...")
except (FileNotFoundError):
    print("No task added.")
except (IndexError):
    help()
